/* the following are available only on the server */

var config = require('../config'),
    fs = require('fs'),
    path = require('path'),
    yaml = require('yaml-front-matter'),
    lunr = require("lunr"),
    dir_data = path.join(__dirname, "..", "..", "data", "users"); // change to bees

exports.idx_bees = lunr(function() {
    this.ref("id"),
    this.field("name", { boost: 10 }),
    this.field("org"),
    this.field("body")
});

populateBeesSearchIndex = function(bee) {
    exports.idx_bees.add({
        "id": bee.id,
        "name": bee.name,
        "org": bee.org,
        "body": bee.body
    });
};

exports.buildBeesCache = function() {
    var bees = [];

    fs.readdirSync(dir_data).filter(function(file, index) {

        // remove entries that start with "."
        if (file.substr(0, 1) !== ".") {

            // return only those directories that have an index.md inside
            var path_to_index = path.join(dir_data, file, "index.md");
            return fs.lstatSync(path_to_index).isFile();
        }
    }).forEach(function(file, index) {

        var bee = {
            i : index,
            id : file,
            url : config.url + "/bees?id=" + file,
        };

        var filename = path.join(dir_data, file, "index.md");

        var metadata = yaml.loadFront(filename);
        if (metadata) {
            bee.name = metadata["name"];
            bee.org = metadata["org"];
            bee.email = metadata["email"];
            bee.body = metadata["__content"];
        }

        populateBeesSearchIndex(bee);
        bees.push(bee);
    });

    return bees;
};

exports.makeHtml = function(data, mielho) {
    if (data.length > 1) {
        for (var i=0, j=data.length; i<j; i++) {

            var txt = mielho.converter.makeHtml(data[i].body);

            if (mielho.lightweight) {
                txt = txt.replace(
                    /<img src=".*?" (.*)>/g,
                    '<img src="/img/2x2.png" height="20" width="100%" border="1">'
                );
            }
            data[i].body = txt;

            // var tags = data[i].tags.map(function(el) {
            //     return {
            //         val : el.replace(/ /g, '+'),
            //         dsp : el,
            //         cls : "not_in_focus"
            //     }
            // });
            // data[i].tags = tags;

            // make the lesson's accordion closed but visible
            data[i].status = "closed";

            // hide the lesson's details
            data[i].visibility = "off";
        }
    }
    else {

        // make the lesson's accordion closed but visible
        data[0].status = "open";

        // hide the lesson's details
        data[0].visibility = "on";
    }

    return {"bees" : data};
};

/**
search for bee by id is always performed against
the entire bees collection
**/
exports.getBeesById = function(id, bees, mielho) {
    var search_result = [];

    for (var i=0, j=bees.length; i<j; i++) {
        if (bees[i].id === id) {
            search_result.push(bees[i]);
            break;
        }
    }

    return search_result;
};

/**
 * search for bee by q is performed against
 * a provided subset of collection. If such a set is
 * not provided then the entire bees collection is used
 */
exports.getBeesByQ = function(q, bees, mielho) {
    var hits = mielho.bees.idx_bees.search(q);
    var search_result = [];
    var with_score = false;

    if (with_score) {

        /*
        return an array of hashes like so

        [
            {
                // record.id === ref
                record: {}
                score: 0.36945354355361815
            }
        ]
        */

        for (var i=0, j=hits.length; i<j; i++) {
            for (var k=0, l=bees.length; k<l; k++) {
                if (hits[i].ref === bees[k].id) {
                    search_result.push({
                        hit: bees[k],
                        score : hits[i].score
                    });
                    continue;
                }
            }
        }
    }
    else {

        /*
        return an array of bees like so

        [
            bee,
            bee,
            …
        ]
        */
        for (var i=0, j=hits.length; i<j; i++) {
            for (var k=0, l=bees.length; k<l; k++) {
                if (hits[i].ref === bees[k].id) {
                    search_result.push(bees[k]);
                    continue;
                }
            }
        }
    }

    return search_result;
};
