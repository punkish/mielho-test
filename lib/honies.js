/* the following are available only on the server */

var config = require('../config'),
    fs = require('fs'),
    path = require('path'),
    yaml = require('yaml-front-matter'),
    lunr = require("lunr"),
    dir_data = path.join(__dirname, "..", "..", "data", "projects");

exports.katex = require("katex");

exports.idx_honies = lunr(function() {
    this.ref("id"),
    this.field("title", { boost: 10 }),
    this.field("tags"),
    this.field("body")
});

populateHoniesSearchIndex = function(honey) {
    exports.idx_honies.add({
        "id": honey.id,
        "title": honey.title,
        "tags": honey.tags.map(function(element) {return element.tag}).join(","),
        "body": honey.body
    });
};

exports.buildHoniesCache = function() {
	var honies = [];

    fs.readdirSync(dir_data).filter(function(file, index) {

        // remove entries that start with "."
        if (file.substr(0, 1) !== ".") {

            // return only those directories that have an index.md inside
            var path_to_index = path.join(dir_data, file, "index.md");
            return fs.lstatSync(path_to_index).isFile();
        }
    }).forEach(function(file, index) {

        var honey = {
            i : index,
            id : file,
            url : config.url + "/honey?id=" + file,
            title : file.replace(/-+/g, ' ').replace(/\\+/g, '')
        };

        var tags_folder = path.join(dir_data, file, "_tags");
        var tags;

        try {
            tags = fs.readdirSync(tags_folder);
        }
        catch (error) {
            // Here you get the error when the file was not found,
            // but you also get any other error

            //console.log("got an error opening _tags for " + f + ": " + error);
        }

        var stars_folder = path.join(dir_data, file, "_stars");
        var stars_obj = {};
        var stars;

        try {
            var stars_tmp = fs.readdirSync(stars_folder);
            stars_tmp.forEach(function(file, index) {
                var star_file = path.join(stars_folder, file);
                var star_val = fs.readFileSync(star_file, "utf-8");
                stars_obj.file = star_val;
            });
            stars = Object.keys(stars_obj);
        }
        catch (error) {
            // Here you get the error when the file was not found,
            // but you also get any other error

            //console.log("got an error opening _stars for " + f + ": " + error);
        }

        var filename = path.join(dir_data, file, "index.md");

        var metadata = yaml.loadFront(filename);
        if (metadata) {

            // map user-entered date to a better one using moment's great parser
            if (metadata.date) {
                honey.date = moment(metadata.date).format();
            }

            honey.user = metadata["researcher"];
            honey.stars = metadata["stars"] || stars;
            honey.tags = metadata["tags"] || tags;
            honey.body = metadata["__content"];
        }

        populateHoniesSearchIndex(honey);
        
        honey.bundle = exports.populateHoneyBundle(file);
        
        honies.push(honey);
    });

	return honies;
};

exports.populateHoneyBundle = function(id) {
    
    var bundle = {
        "venom" : [],
        "pollen" : [],
        "wax" : []
    };
    
    var bundle_parts = Object.keys(bundle);
    for (var i=0, j=bundle_parts.length; i<j; i++) {
        var element = bundle_parts[i];
        var part_dir = path.join(dir_data, id, element);
        var parts = [];
        
        try {
            
            if (fs.lstatSync(part_dir).isDirectory()) {
                fs.readdirSync(part_dir).filter(function(file, index) {
                    
                    // remove entries that start with "."
                    if (file.substr(0, 1) !== ".") {
        
                        // return parts inside
                        var part_file = path.join(dir_data, id, element, file);
                        
                        var part = fs.readFileSync(part_file, "utf-8");
                        var this_part = {
                            "file" : file,
                            "content" : part,
                            "type" : path.extname(part_file).replace(/^\./, "")
                        };
        
                        bundle[element].push(this_part);
                    }
                });
            }
        }
        catch (error) {
            // Here you get the error when the file was not found,
            // but you also get any other error
        
            //console.log("got an error opening _stars for " + f + ": " + error);
        }
    }
    
    return bundle;
};


exports.makeHtml = function(cached_data, mielho) {
    var data = JSON.parse(JSON.stringify(cached_data));

    for (var i=0, j=data.length; i<j; i++) {

        var txt = mielho.converter.makeHtml(data[i].body);

        if (mielho.lightweight) {
            txt = txt.replace(
                /<img src=".*?" (.*)>/g,
                '<img src="/img/2x2.png" height="20" width="100%" border="1">'
            );
        }
        data[i].body = txt;

        var tags = data[i].tags.map(function(el) {
            return {
                val : el.replace(/ /g, '+'),
                dsp : el,
                cls : "not_in_focus"
            }
        });
        data[i].tags = tags;
            
        // make the accordion closed but visible
        data[i].status = "closed";
        
        // hide the record's details
        data[i].visibility = "off";
    }
    
    if (data.length == 1) {
        
        // make the accordion open
        data[0].status = "open";
        
        // show the record's details
        data[0].visibility = "on";
        
        if (data[0].bundle.pollen) {
            if (data[0].bundle.pollen[0].type === "tex") {
                var foo = data[0].bundle.pollen[0].content;
                data[0].bundle.pollen[0].content = mielho.honies.katex.renderToString(foo);
            }
        }
    }
    
    return {"honies" : data};
};

/**
search for bee by id is always performed against
the entire bees collection
**/
exports.getHoniesById = function(id, honies, mielho) {
    var search_result = [];
    
    for (var i=0, j=honies.length; i<j; i++) {
        if (honies[i].id === id) {
            search_result.push(honies[i]);
            break;
        }
    }

    //console.log(search_result);
    return search_result;
};

/**
search for honies by tags is performed against a provided subset of
collection. If such a set is not provided then the entire lessons
collection is used
@param {string} tags - semi-colon separated list of tags
@param {Object[]} lessons - collection of lessons or null
**/
exports.getHoniesByTags = function(tags, honies, mielho) {
    if (tags === "all") {
        return honies;
    }
    else {            
        var search_result = null;

        var _findByTags = function(tags_arr, honies_arr) {
            var tmp_honies = [];
            var t = tags_arr.shift();

            for (var i=0, j=honies_arr.length; i<j; i++) {
                for (var k=0, l=honies_arr[i].tags.length; k<l; k++) {
                    if (honies_arr[i].tags[k] === t) {
                        tmp_honies.push(honies_arr[i]);
                    }
                }
            }

            if (tags_arr.length > 0) {
                _findByTags(tags_arr, tmp_honies);
            }
            else {
                search_result = tmp_honies;
            }
        };

        var tags_arr = tags.split(";");
        _findByTags(tags_arr, honies);
        
        return search_result;
        // return {
        //     "search_result" : search_result,
        //     "tags" : tags_arr
        // }
    }
};

/**
 * search for honey by q is performed against
 * a provided subset of collection. If such a set is
 * not provided then the entire honies collection is used
 */
exports.getHoniesByQ = function(q, honies, mielho) {
    var hits = mielho.honies.idx_honies.search(q);
    var search_result = [];
    var with_score = false;

    if (with_score) {

        /*
        return an array of hashes like so

        [
            {
                // record.id === ref
                record: {}
                score: 0.36945354355361815
            }
        ]
        */

        for (var i=0, j=hits.length; i<j; i++) {
            for (var k=0, l=honies.length; k<l; k++) {
                if (hits[i].ref === honies[k].id) {
                    search_result.push({
                        hit: honies[k],
                        score : hits[i].score
                    });
                    continue;
                }
            }
        }
    }
    else {

        /*
        return an array of honies like so

        [
            honey,
            honey,
            …
        ]
        */
        for (var i=0, j=hits.length; i<j; i++) {
            for (var k=0, l=honies.length; k<l; k++) {
                if (hits[i].ref === honies[k].id) {
                    search_result.push(honies[k]);
                    continue;
                }
            }
        }
    }

    return search_result;
};

/**
search for bee by user
**/
exports.getHoniesByUser = function(user, honies, mielho) {
    var search_result = [];
    
    for (var i=0, j=honies.length; i<j; i++) {
        if (honies[i].user === user) {
            search_result.push(honies[i]);
        }
    }

    return search_result;
};
