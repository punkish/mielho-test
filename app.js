/*
Start this program from the command line with `pm2`

    ~/Nodes/pk$ NODE_ENV=production pm2 restart app.js
*/

var config = require("./config"),
	express = require("express"),
	path = require("path"),
	moment = require("moment"),
	fs = require("fs"),
	Hogan = require("hogan.js"),
	dir_data = path.join(__dirname, "..", "data"),
	dir_public = path.join(__dirname, "public");

// import the resource modules server side code
var mielho = require("./lib/mielho.js");

/**
import the resource modules server+client side code for /public/js/main.js
and merge it into the "mielho" namespace
**/
var tmp = require("./public/js/mielho.js");
for (var attrname in tmp) {
	mielho[attrname] = tmp[attrname];
}

// Now do the same for other dynamic resources
var resources = mielho.resources;
var dynamic_resources = [];
for (var resource in resources) {
	if (resources[resource].resource_type === "dynamic") {
		dynamic_resources.push(resource);
		mielho[resource] = require("./lib/" + resource + ".js");

		var tmp = require("./public/js/" + resource + ".js");
		for (var attrname in tmp) {
			mielho[resource][attrname] = tmp[attrname];
		}
	}
}
// console.log(mielho);

var layouts = {
	mielho : Hogan.compile(fs.readFileSync(path.join(__dirname, "views", "layouts", "mielho.mustache"), "utf8")),
	one_mielho : Hogan.compile(fs.readFileSync(path.join(__dirname, "views", "layouts", "one_mielho.mustache"), "utf8"))
};


/**
When the application starts, it queries the data store and builds caches
and search indexes (where applicable).
**/
mielho.buildDataCaches(mielho);

var app = express();

app.enable("trust proxy");

//app.use(logger(logtype, {stream: accessLog}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.static(dir_public));
app.use(express.static(dir_data));

/*
The order of which middleware are "defined" using app.use()
is very important, they are invoked sequentially, thus this
defines middleware precedence. For example usually
express.logger() is the very first middleware you would
use, logging every request EXCEPT static files
*/

app.get("/:resource?", function (req, res) {

	/**
	The very first task is to figure out what is being requested and if
	there are any query parameters.
	**/
	var query_object = mielho.makeQueryObjectFromRequest(req, mielho);
	var resource = query_object.resource;
	var resource_type = resources[resource].resource_type;

	/**
	Now we query for the data based on the query object, get
	back the result, and package it as a minimal data package
	with the name of the resource. This is already appropriate
	to be returned as JSON in case of an API query (see below)
	**/
	var data = {
		"resource" : resource,
		"value" : mielho.getResource(query_object, mielho)
	};

	/**
	An incoming query is either for data (an API query) or for a web page.
	We can find out which based on request header.
	**/
	if (req.headers["content-type"] === "application/json;charset=UTF-8") {

		// Since the content-type requested is JSON, we send back JSON
		res.json(data);
	}

	/**
	On the other hand, if the request header wants back HTML or XML, it is a
	web request, so we format it as HTML, and then send the html back.
	**/
	else {
		
	    var resource_html = mielho.packageDataAsHTML({
		    data : data, 
		    resource_type : resource_type,
		    query_params : query_object.query_params,
		    mielho : mielho
		});
		
		var html;
		var layout_html = {
			"app" : config.app,
			"resource" : resource,
			"init" : (resource_type === "dynamic" ? true : false),
		
			// makes the nav bar in the web page
			"dynamic-resources" : dynamic_resources
		};
		if (query_object.query_params.id) {
			
			var view = fs.readFileSync(path.join(__dirname, "views", "one_honies.mustache"), "utf8");
			var compiled_view = Hogan.compile(view);
			var view_content = {"content" : compiled_view.render(resource_html)};
									
			// combine resource-specific html with layout html
			html = layouts.one_mielho.render(layout_html, view_content);
		}
		else {
			
			var view = fs.readFileSync(path.join(__dirname, "views", mielho.resources[resource].view + ".mustache"), "utf8");
			var compiled_view = Hogan.compile(view);
			var view_content = {"content" : compiled_view.render(resource_html)};

			// combine resource-specific html with layout html
			html = layouts.mielho.render(layout_html, view_content);
		}
		
		res.send(html);
	}
});

app.use(function(req, res) {
    res.status(404).send(config["404"]);
});

app.listen(config.port, function () {
    console.log("Listening on port " + config.port);
});
