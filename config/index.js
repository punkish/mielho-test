module.exports = (function(mode) {
    var config = {
        base: {
            app : {
                name: "mielho",
                desc: "modern information environment for listing honeybee output"
            },
            "404": "Sorry can’t find that!",
            public_dir: "public",
            data_dir: "data",
            baseuri: "",
            extension: ".md"
        },
        development: {
            mode: "development",
            port: 3000,
            production: false,
            url: "http://localhost:3000"
        },
        production: {
            mode: "production",
            port: 4000,
            production: true,
            url: "http://mielho.punkish.org"
        }
    };

    var tmp = config[process.env.NODE_ENV || "development"];
    for (var i in config.base) {
        tmp[i] = config.base[i];
    }

    return tmp;
})();
