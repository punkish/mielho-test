/* only in the browser */
var mielho = {

    // A jQuery-style selector without the cruft of jQuery
    $ : function(el) {
        if (el.substr(0, 1) === "#") {
            return document.getElementById(el.substr(1));
        }
        else if (el.substr(0, 1) === ".") {
            return document.getElementsByClassName(el.substr(1));
        }
        else {
            return document.getElementsByTagName(el);
        }
    },

    supports_history_api : function() {
        return !!(window.history && history.pushState);
    },

    lightweight : false,

    // parseUri 1.2.2
    // (c) Steven Levithan <stevenlevithan.com>
    // MIT License
    // http://blog.stevenlevithan.com/archives/parseuri
    parseUri : function(str) {
        var	o = {
            strictMode: false,
            key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
            q: {
                name:   "queryKey",
                parser: /(?:^|&)([^&=]*)=?([^&]*)/g
            },
            parser: {
                strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
            }
        };

        var m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
            uri = {},
            i   = 14;

        while (i--) uri[o.key[i]] = m[i] || "";

        uri[o.q.name] = {};
        uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
            if ($1) uri[o.q.name][$1] = $2;
        });

        return uri;
    },

    // titleCase : function(txt) {
    //     return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    // },

    xhr : function(url, reqListener) {

        //if (mielho.resources.dynamic.indexOf(resource) != -1) {
            //mielho.hideSections();
            //mielho.$("#" + resource + "-section").className = "on";
        //}

        oReq = new XMLHttpRequest();
        oReq.addEventListener("load", reqListener);
        oReq.open("GET", url);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        oReq.send();
    },

    activateLinks : function(class_name) {
        var links = mielho.$(class_name);
        for (var i=0, j=links.length; i<j; i++) {
            links[i].addEventListener("click", mielho.foo);
        }
    },
    
    // reset the search form if it exists (some webpages may not have)
    activateSearch : function(resource) {
    
    	var q = mielho.$("#q");
    
    	if (q) {
    		q.focus();
    
    		q.addEventListener(
    			"keyup",
    			mielho.debounce(function() {
    				if (this.value.length > 3) {
    					var uri = "/" + resource + "?q=" + this.value;
    					mielho.xhr(resource, uri, function() {
    						if (this.response) {
    							var resp = JSON.parse(this.response);
    							var ids = resp.value.map(function(e) {
    								return e.id;
    							});
    
    							var list = mielho.$("#" + resource);
    							var arr = [].slice.call(list.children);
    
    							arr.forEach(function(e, i) {
    								if (ids.indexOf(e.id) == -1) {
    									e.className = "entry closed off";
    								}
    								else {
    									e.className = "entry closed on";
    								};
    							})
    
    							mielho.$("#num").innerHTML = ids.length;
    							// mielho.$("#total").innerHTML = mielho.resources[resource].cache.full.length;
    
    							history.pushState(null, uri, uri);
    						}
    					});
    				}
    				else if (this.value === "") {
    					var list = mielho.$("#" + resource);
    					var arr = [].slice.call(list.children);
    
    					arr.forEach(function(e, i) {
    						e.className = e.tagName === "DT" ? "entry closed on" : "off";
    					})
    
    					mielho.$("#num").innerHTML = mielho.resources[resource].cache.full.length;
    					// mielho.$("#total").innerHTML = mielho.resources[resource].cache.curr.length;
    
    					history.pushState(null, "/" + resource, "/" + resource);
    				}
    			}, 250)
    		);
    	}
    },
    
    open_accordion_idx : null,
    
    activateAccordion : function() {
    	var list = mielho.$(".entry");
    
    	if (list.length > 1) {
    		for (i=0, j=list.length; i<j; i++) {
    
    			list[i].addEventListener("click", function(e) {
    				
    				if (mielho.open_accordion_idx) {
    					list[mielho.open_accordion_idx].className = "entry closed";
    					list[mielho.open_accordion_idx].nextElementSibling.className = "off";
    				}
    				
    				mielho.open_accordion_idx = this.dataset.val;
    				
    				this.className = "entry open";
    				this.nextElementSibling.className = "on";
    				
    				history.pushState(null, this.id, "#" + this.id);
    			
    			});
    		}
    	}
    },

    writeEmptyResource : function(resource) {

        // First, hide all the sections
        mielho.hideSections();

        var data = {"accounts-form-type" : resource.replace(/-/g, ' ')};
        data[resource] = true;

        mielho.$("#accounts-section").innerHTML = templates.accounts.render(data);

        // Now show the resource
        mielho.$("#accounts-section").className = "on";
        mielho.activateLinks(".view");
    },

    writeStaticResource : function(resource) {

        // First, hide all the sections
        mielho.hideSections();

        // Now write the content and show it
        mielho.$("#static-resource-section").innerHTML = main[resource];
        mielho.$("#static-resource-section").className = "on";
        mielho.activateLinks(".view");
    },

    hideSections : function() {
        [].slice.call(mielho.$("section")).forEach(function(element, index) {
            element.className = "off";
        });
    },
    
    /*
    Returns a function, that, as long as it continues to be
    invoked, will not be triggered. The function will be called
    after it stops being called for N milliseconds. If `immediate`
    is passed, trigger the function on the leading edge, instead
    of the trailing.
    */
    debounce : function(func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) func.apply(context, args);
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow) func.apply(context, args);
    	};
    }
};

// Writing for node and the browser
// http://caolan.org/posts/writing_for_node_and_the_browser/

// the following will be used both server- and client-side
(function(exports) {

    /**
    Canonical list of resources that are delivered by this application.
    Each resource is what appears in the URL at the top level, after the
    domain, and can be accessed either by loading the webpage or by
    querying the API.
    **/
    exports.resources = {
    	"about" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"copyright" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"index" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"legalese" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : []
    	},
    	"api" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"privacy-statement" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"terms-of-service" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"terms-of-use" : {
        	"view" : "static",
    		"resource_type" : "static",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"site-map" : {
        	"view" : "static",
    		"resource_type" : "programmatic",
    		"cache_it" : false,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["name"]
    	},
    	"create-account" : {
        	"view" : "accounts",
    		"resource_type" : "empty",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : []
    	},
    	"login" : {
        	"view" : "accounts",
    		"resource_type" : "empty",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : []
    	},
    	"honies" : {
        	"view" : "honies",
    		"resource_type" : "dynamic",
    		"cache_it" : true,
    		"cache" : {
    			"full" : null,
    			"curr" : null,
    		},
    		"valid_query_params" : ["id", "tags", "q", "user"]
    	},
    	"bees" : {
        	"view" : "honies",
    		"resource_type" : "dynamic",
    		"cache_it" : true,
    		"cache" : {
    		    "full" : null,
    		    "curr" : null,
    	},
    		"valid_query_params" : ["id", "q"]
    	}
    };

})(typeof exports === "undefined" ? this["mielho"] = mielho : exports);
