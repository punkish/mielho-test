/* only in the browser */

var honies = {
    
    init : function() {
        mielho.activateAccordion();
        mielho.activateSearch("honies");
    },
    
    loadHoneyBundles : function() {
        mielho.xhr("/honey-pot?honey_id=");
    },
    
    addRemoveTag : function(tag, action) {
        var uri = mielho.parseUri(document.location);
    
        var other_query_keys = [];
        var query_key_tags = [];
    
        for (var k in uri.queryKey) {
            if (k === "tags") {
                query_key_tags = uri.queryKey["tags"].split(";");
            }
            else {
                other_query_keys.push(k + "=" + uri.queryKey[k]);
            }
        }
    
        var tag_query = "";
    
        // add tag
        if (action === "add") {
    
            // add the tag *only* if it doesn't already exist in
            // the query string
            if (query_key_tags.indexOf(tag) == -1) {
                query_key_tags.push(tag);
            }
    
            tag_query = "tags=" + query_key_tags.join(";");
    
        // remove tag
        }
        else if (action === "remove") {
            //PK.curr_lessons = PK.lessons;
    
            var index = query_key_tags.indexOf(tag);
            if (index > -1) {
                query_key_tags.splice(index, 1);
            }
    
            if (query_key_tags.length > 0) {
                tag_query = "tags=" + query_key_tags.join(";");
            }
        }
    
        other_query_keys.push(tag_query);
    
        var query = other_query_keys.join("&");
    
        var href = uri.path + (query ? ("?" + query) : "");
        history.pushState(null, href, href);
        honies.writeHonies();
    },
    
    writeHonies : function(q) {
        //PK.curr_lessons = PK.filterLessons(PK.curr_lessons, q);
    
        // First, hide all the sections
        // PK.hideSections();
    
        // Write tags above the resources. This *only* takes place when there
        // are tags in the URI, that is, when the user has asked for
        // resources with certain tags
        var tags_arr = honies.writeTags();
    
        // Now rejig the lessons data object with values for tag_class
        // so tags can be colored yellow or not
        if (typeof(tags_arr) !== "undefined") {
            for (var i=0, j=PK.curr_lessons.length; i<j; i++) {
                var tags = PK.curr_lessons[i].tags;
                for (var k=0, l=tags.length; k<l; k++) {
                    tags[k].cls = tags_arr.indexOf(tags[k].val) != -1 ? "in_focus" : "not_in_focus";
                }
            }
        }
    
        // write the lessons
        // PK.$("#lessons-data").innerHTML = Mustache.to_html(
        //     PK.$("#lessons-template").innerHTML, {
        //         "num" : PK.curr_lessons.length,
        //         "total" : PK.lessons.length,
        //         "entries" : PK.curr_lessons
        //     }
        // );
    
        PK.$("#lessons-section").className = "on";
        PK.highlightSyntax();
        PK.activateLessonsAccordion();
        PK.activateLinks(".view");
    
        var uri = PK.parseUri(document.location);
        var anchor = uri.anchor || "";
        if (anchor) {
            var now = document.getElementById(anchor).getBoundingClientRect().top;
            PK.smoothScroll(now);
        }
    
    },
    
    writeTags : function() {
    
        // Figure out if there are any tags in the URI
        var uri = PK.parseUri(document.location);
    
        if (uri.queryKey.tags) {
            var tags = uri.queryKey.tags;
            if (tags.toLowerCase() === "all") {
                PK.$("#tags").innerHTML = "";
                return [];
            }
            else {
                var tags_arr =  tags.split(";")
    
                                // make the elements unique
                                // http://stackoverflow.com/questions/1960473/unique-values-in-an-array
                                // if ['foo', 'bar', 'foo', 'baz']
                                // → ['foo', 'bar', 'baz']
                                .filter(function(value, index, self) {
                                    return self.indexOf(value) === index;
                                });
    
                PK.$("#tags").innerHTML = Mustache.to_html(
                    PK.$("#tags-template").innerHTML, {
                        tags: tags_arr.map(function(el) {
                            return {
                                val : el,
                                dsp : el.replace(/\+/g, ' '),
                                cls : ''
                            }
                        })
                    }
                );
    
                return tags_arr;
            }
        }
        else {
            PK.$("#tags").innerHTML = "";
            return [];
        }
    }

};

/* in both the browser and the server */
(function(exports) {

    


})(typeof exports === "undefined" ? this["honies"] = honies : exports);
